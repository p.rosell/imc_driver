# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

- Adds simple position mode example as a ROS node.

### Changed

- Updated doc link at readme.

## [1.1.0] - 2020-01-09

### Added

- Adds latest release and doc links at readme.
- Resets controllers when some drive is not in operation enable, so we start controllers with fresh inited commands.
- Exposes profile velocity and position range limits parameters.
- Adds software safety layer through configurable max allowed velocity, applicable to all operation modes.

### Changed

- Launches imc_driver node with its own name, inside provided namespace.
- Set BUILD_DOCUMENTATION option to false by default.

### Fixed

- Node handles are now initialized without additional namespace, which was causing problems to get params.
- Corrects max-min position range limit idx.

## [1.0.1] - 2019-11-18

### Fixed
- Fixed unwanted motion that occurred when switching from relative back to absolute positioning mode.

## [1.0.0] - 2019-11-06

### Added

- Initial release

[unreleased]: https://gitlab.com/beta-robots/imc_driver/tree/devel
[1.0.0]: https://gitlab.com/beta-robots/imc_driver/-/tags/1.0.0
[1.0.1]: https://gitlab.com/beta-robots/imc_driver/-/tags/1.0.1
[1.1.0]: https://gitlab.com/beta-robots/imc_driver/-/tags/1.1.0
