<div align="center">
![Logo](media/IMCdrives.png)
</div>

<br>

[![latest release](https://img.shields.io/badge/latest%20release-1.0.1-blue.svg)](https://gitlab.com/beta-robots/imc_driver/-/tags/1.0.1)
[![doc for release](https://img.shields.io/badge/doc%20for%20release-1.0.1-blue)](http://www.beta-robots.com/IMCDrives/doc)

# Overview

This repository contains a C++ library and a  ROS package to interface with [IMC servo drives](https://ingeniamc.com/) through [EtherCAT (CoE profile)](https://www.ethercat.org/en/technology.html).

Contents:

1. [Software architecture](#software-architecture)
1. [Building the package](#building-the-package)
1. [imc_driver library usage](#imc_driver-library-usage)
1. [ROS usage](#ros-usage)
1. [ROS API](#ros-api)
1. [ROS tutorial](#ros-tutorial)
1. [How this code has been tested](#how-this-code-has-been-tested)
1. [Acknowledgements](#acknowledgements)

# Software architecture

This package builds a C++ shared library called **imc_driver** and a ROS executable node called **imc_hwiface_node**. As shown in the diagram below , the library is built on top of the external  [SOEM](https://openethercatsociety.github.io/) library, while the ROS node depends on the  [ros_control](http://wiki.ros.org/ros_control) ROS package set.  

<div align="center">
![Software Diagram](media/sw_diagram.png)
</div>

# Building the package
First of all you need to install the dependencies:

#### SOEM dependency
The following steps are required to clone and build the SOEM library, from a forked repository:
```
git clone https://github.com/beta-robots/SOEM-1.git
cd SOEM-1 && git checkout 5b7b9e717059fd377fb3f32770bb5167da1a4dd8
mkdir build && cd build && cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=/usr/local/ .. && make
sudo make install
```
#### ROS Control dependencies
```
sudo apt install ros-melodic-controller-manager
sudo apt install ros-melodic-hardware-interface
sudo apt install ros-melodic-joint-limits-interface
```

#### Building the ROS package (library and node)
Finally, the ROS package can be built through catkin system, as any standard ROS package. This will build and install both, the library and the node.

Since the SOEM library uses raw sockets, the CMake installation executes a script to set capabilities to the built binaries in order to allow raw sockets on run time.

# imc_driver library usage

[This example](src/examples/library_device_example.cpp) shows a general use of the library from a C++ main() program. You can execute it by typing:
```
rosrun imc_driver library_device_example [eth_if_name] [num_of_devices]
```
[Another example](src/examples/quick_stop_example.cpp) shows how to use the quick stop feature through the library. It can be called with:
```
rosrun imc_driver quick_stop_example [eth_if_name] [num_of_devices]
```

# ROS usage

#### Before running the package in ROS

Since the node uses the SOEM library, it requires some privileges to run. The CMakeLists of this package already includes instructions to set these permissions automatically
when compiling with install option, so expect a prompt requesting your sudo password. However, if you need to do these manually, you can:

1. After node compilation, go to the exectuable folder by typing:
```
cd devel/lib/imc_driver
```
2. Then, change the privileges:
```
sudo setcap cap_net_raw+ep imc_hwiface_node
```

For more information, see the [SOEM manual](https://openethercatsociety.github.io/doc/soem/index.html ), where it states that it uses RAW sockets and, therefore, will need to run as root.

#### Configuration files

At *config/hwiface.yaml* file, there are the following parameters to configure:
 - rate_in_hz: read/write loop rate [hz]
 - joint_names: string vector of joint names you want to move with the hardware interface, as defined in your robot description.
 - eth_if_name: name of the ethernet interface device. You can find out it typing in a terminal the command `ifconfig -a`.
 - driver_verbose: whether the execution of the ros node will provide debugging messages from the imc_driver library or not.
 - default_max_allowed_torque: max allowed torque for each device in per mil of the motor rated torque.
 - default_profile_acceleration: profile acceleration for each device in rad/s^2.
 - default_profile_deceleration: profile deceleration for each device in rad/s^2.
 - default_max_acceleration: max acceleration for each device in rad/s^2.
 - default_max_deceleration: max deceleration for each device in rad/s^2.
 - default_max_profile_velocity: max profile velocity for each device in rad/s.
 - default_positioning_relative: positioning mode for each device. True means relative, false means absolute.

The default parameters are read and configured at the devices at node initalization. The devices configuration can be later on updated using the 'SetIMCDriverParameters' service.

In order to run the ROS controllers associated to each joint, you should edit the *config/controllers.yaml* file and list the joint names of your robot, as well as add/remove controllers of your choice.

#### Launch files

This package comes with three launch files:

1. *imc_driver.launch*: launches the hardware interface ROS node.
1. *controllers.launch*: launches the joint state monitor and the position, velocity (default), and effort controllers.
1. *imc_driver_with_controllers.launch*: launches the previous two launch files.

#### IMPORTANT NOTE: Runtime linking issue

The following runtime linking error has been seen to appear with Ubuntu 18.04 and ROS Melodic:
```
.../imc_hwiface_node: error while loading shared libraries: libroscpp_serialization.so: cannot open shared object file: No such file or directory
```

and the node dies. If this happens, you can try the following workaround according to [this discussion](https://stackoverflow.com/questions/9843178/linux-capabilities-setcap-seems-to-disable-ld-library-path) (see user2706978 reply):

```
cd /etc/ld.so.conf.d
```
and add the following lines to the file libroscpp.conf:
```
/opt/ros/melodic/lib
/usr/lib/x86_64-linux-gnu
/lib/x86_64-linux-gnu
```
save and quit, and then
```
sudo ldconfig
```
so you explicitly tell to the linker where to look for libroscpp dependencies.

# ROS API

The executable ROS node inherits from the ROS hardware_interface, so it fulfills the ROS API described [here](http://wiki.ros.org/ros_control#Overview). Additionally, the following API is available:

## Published topics

- *imc_driver_state* (imc_driver/IMCDriver): status of the imc_driver library and devices. Includes overall status and each device's driver state, ethercat state, and last know error.

## Services

- *quick_stop_request* (std_srvs/Trigger): Requests a quick stop on all devices.
- *rearm_request* (std_srvs/Trigger): Requests an attempt to enable all devices.
- *latch_relative_position* (std_srvs/Trigger): Triggers the start of the relative motion towards the last received position command for all devices in relative positioning mode.
- *parameters_server* (imc_driver/SetIMCDriverParameters): Allows to update configuration parameters at the devices.

[This snippet](https://gitlab.com/beta-robots/imc_driver/snippets/1910817) shows how to use the *parameters_server* to update devices configuration from within a ROS node.

# ROS tutorial

A simple usage example of the ROS node with the provided controllers and two motors is the following:

* On a terminal, start the node and controllers:
    ```
    roslaunch imc_driver imc_driver_with_controllers.launch
    ```

* On another terminal, send a velocity command:
    ```
    rostopic pub /my_imc_driver/joint_velocity_streamer/command std_msgs/Float64MultiArray "layout:
      dim:
      - label: ''
        size: 0
        stride: 0
      data_offset: 0
    data: [0.5, -0.5]"
    ```
    The two motors should now turn at 0.5 rad/s in opposite directions.

* Stop the motors by sending zero velocity:
    ```
    rostopic pub /my_imc_driver/joint_velocity_streamer/command std_msgs/Float64MultiArray "layout:
      dim:
      - label: ''
        size: 0
        stride: 0
      data_offset: 0
    data: [0.0, 0.0]"
    ```

* Switch to position controller:
    ```
    rosservice call /my_imc_driver/controller_manager/switch_controller "start_controllers:
    - 'joint_position_streamer'
    stop_controllers:
    - 'joint_velocity_streamer'
    strictness: 1"
    ```

* When you power up the system, the position state of the joints is set at zero. Since we already moved the joints in velocity command, the current position state can be inspected with:
    ```
    rostopic echo /my_imc_driver/joint_states
    ```
    and you should see and output like this:
    ```
    header:
      seq: 5113
      stamp:
        secs: 1569935859
        nsecs: 164449583
      frame_id: ''
    name: [joint_1, joint_2]
    position: [6.451923193847007, -6.454991155422778]
    velocity: [0.0, 0.0]
    effort: [0.0, -0.036]
    ```
    Our joints are at 6.45 rad and -6.45 rad from the original position.

* Let's get the joints back to the original position with position control:
    ```
    rostopic pub /my_imc_driver/joint_position_streamer/command std_msgs/Float64MultiArray "layout:
      dim:
      - label: ''
    	size: 0
    	stride: 0
      data_offset: 0
    data: [0.0, 0.0]"
    ```

* And checking again with an echo of the joint_states topic should give something like:
    ```
    header:
      seq: 15166
      stamp:
        secs: 1569936060
        nsecs: 324464431
      frame_id: ''
    name: [joint_1, joint_2]
    position: [0.0, 0.0]
    velocity: [0.0, 0.0]
    effort: [0.017, 0.01575]
    ```

* Now, we can make the two joints spin 50 degrees (0.873 radians):
    ```
    rostopic pub /my_imc_driver/joint_position_streamer/command std_msgs/Float64MultiArray "layout:
      dim:
      - label: ''
    	size: 0
    	stride: 0
      data_offset: 0
    data: [0.873, 0.873]"
    ```

* Now, switch to effort control:
    ```
    rosservice call /my_imc_driver/controller_manager/switch_controller "start_controllers:
    - 'joint_effort_streamer'
    stop_controllers:
    - 'joint_position_streamer'
    strictness: 1"
    ```

* And tell the motors to sustain a 0 Nm torque:
    ```
    rostopic pub /my_imc_driver/joint_effort_streamer/command std_msgs/Float64MultiArray "layout:
      dim:
      - label: ''
        size: 0
        stride: 0
      data_offset: 0
    data: [0,0]"
    ```
You should be able now to move the motor axis manually, since they are not withstanding any external effort.

* Finally, make the motors sustain a 0.1 Nm torque in opposite directions:
    ```
    rostopic pub /my_imc_driver/joint_effort_streamer/command std_msgs/Float64MultiArray "layout:
      dim:
      - label: ''
    	size: 0
    	stride: 0
      data_offset: 0
    data: [-0.1,0.1]"
    ```
    **WARNING**: If there is no load in your motors, this could make them accelerate very fast. Take any safety measures needed.

# How this code has been tested

This software has been tested using JUP-30/130-E, and JUP20/80-E [IngeniaMC](https://ingeniamc.com/) drives, with DB80L048030-ENM05J, DB80M048030-ENM05J, and DB80C048030-ENM05J [Nanotec](https://en.nanotec.com/) motors.
Up to six motors have been simultaneously controlled in position, velocity, and effort modes.

# Acknowledgements

<table>
  <tr>
    <td align="center">
        <a href="http://www.beta-robots.com">
        <img src="media/beta-robots.png"  alt="beta_robots_logo" height="60" hspace="10" >
        </a>
    </td>
    <td>Developed, maintained and partially supported by [Beta Robots](http://www.beta-robots.com) with private funds.  </td>
  </tr>
  <tr>
    <td align="center">
        <a href="http://rosin-project.eu">
        <img src="http://rosin-project.eu/wp-content/uploads/rosin_ack_logo_wide.png"  alt="rosin_logo" height="60" hspace="10" >
        </a>
    </td>
    <td>Supported by ROSIN - ROS-Industrial Quality-Assured Robot Software Components. More information: <a href="http://rosin-project.eu">rosin-project.eu</a>  </td>
  </tr>
  <tr>
    <td align="center"><img src="http://rosin-project.eu/wp-content/uploads/rosin_eu_flag.jpg" alt="eu_flag" height="45" hspace="10"></td>
    <td>ROSIN project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement no. 732287.</td>
  </tr>
</table>
