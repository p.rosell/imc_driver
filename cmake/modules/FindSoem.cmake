# - Try to find soem
# Once done this will define
#
#  Soem_FOUND - soem found
#  Soem_INCLUDE_DIRS - the soem include directory
#  Soem_LIBRARIES - soem library
#

FIND_PATH(Soem_INCLUDE_DIRS NAMES ethercat.h
  PATHS
  ${Soem_PACKAGE_PATH}/src
  ENV CPATH
  /usr/include/
  /usr/include/soem/
  /usr/local/include/
  /usr/local/include/soem/
  /opt/local/include/
  /opt/local/include/soem/
  NO_DEFAULT_PATH
)

message("Soem_INCLUDE_DIRS is ${Soem_INCLUDE_DIRS}")

FIND_LIBRARY(Soem_LIBRARIES NAMES soem
  PATHS
  ${Soem_PACKAGE_PATH}/lib
  ENV LD_LIBRARY_PATH
  ENV LIBRARY_PATH
  /usr/lib
  /usr/local/lib
  /opt/local/lib
  NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set Soem_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Soem DEFAULT_MSG
Soem_LIBRARIES Soem_INCLUDE_DIRS)

# show the Soem_INCLUDE_DIRS and Soem_LIBRARY_DIR variables only in the advanced view
IF (Soem_FOUND)
  MARK_AS_ADVANCED(Soem_INCLUDE_DIRS Soem_LIBRARIES)
ENDIF (Soem_FOUND)
