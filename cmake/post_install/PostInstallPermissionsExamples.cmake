# This is to set permissions as required by SOEM
execute_process(COMMAND sudo setcap cap_net_raw=eip ${IMC_EXECUTABLE_DIR}/raw_device_example)
execute_process(COMMAND sudo setcap cap_net_raw=eip ${IMC_EXECUTABLE_DIR}/library_device_example)
execute_process(COMMAND sudo setcap cap_net_raw=eip ${IMC_EXECUTABLE_DIR}/sync_motor_example)
execute_process(COMMAND sudo setcap cap_net_raw=eip ${IMC_EXECUTABLE_DIR}/quick_stop_example)
execute_process(COMMAND sudo setcap cap_net_raw=eip ${IMC_EXECUTABLE_DIR}/device_info_example)
