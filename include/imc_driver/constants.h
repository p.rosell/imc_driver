#ifndef ___IMC_DRIVER___CONSTANTS_H___
#define ___IMC_DRIVER___CONSTANTS_H___

namespace imc_driver
{

#define EC_TIMEOUTMON 500

//! Drive's operation modes enumeration.
/*! Available operation modes for imc_drives. You can check
the actual supported supported drive modes with imc_driver::MultiDevice::getSupportedDriveModes*/
enum OperationMode
{
	OPEN_LOOP_VECTOR = -2,
	OPEN_LOOP_SCALAR = -1,
	PROFILE_POSITION = 1,
	VELOCITY = 2,
	PROFILE_VELOCITY = 3,
	PROFILE_TORQUE = 4,
	HOMING = 6,
	INTERPOLATED_POSITION = 7,
	CYCLIC_SYNC_POSITION = 8,
	CYCLIC_SYNC_VELOCITY = 9,
	CYCLIC_SYNC_TORQUE = 10
};

/*! \cond PRIVATE */
enum StateMachineCommand
{
	SHUTDOWN = 1,
	SWITCH_ON = 2,
	SWITCH_ON_PLUS_ENABLE_OPERATION = 3,
	DISABLE_VOLTAGE = 4,
	QUICK_STOP = 5,
	DISABLE_OPERATION = 6,
	ENABLE_OPERATION = 7,
	FAULT_RESET_RISE_EDGE = 8,
	FAULT_RESET_FALL_EDGE = 9
};
/*! \endcond */

//! Drive's state machine states enumeration.
/*! These states are defined by the CiA402 standard (except ::UNKNOWN, which is used when the state is not known).*/
enum StateMachineState
{
	UNKNOWN = 0,
	NOT_READY_TO_SWITCH_ON = 1,
	SWITCH_ON_DISABLED = 2,
	READY_TO_SWITCH_ON = 3,
	SWITCHED_ON = 4,
	OPERATION_ENABLED = 5,
	QUICK_STOP_ACTIVE = 6,
	FAULT_REACTION_ACTIVE = 7,
	FAULT = 8
};

//! IMCDriver states enumeration.
enum MultiDeviceState
{
	ENABLED = 1,	/*!< All devices in ::OPERATION_ENABLED and no errors */
	DISABLED = 2,	/*!< No errors, but not all devices in ::OPERATION_ENABLED */
	ERROR = 3		/*!< Some device in ::FAULT */
};

/*! \cond PRIVATE */
enum MultiDeviceCommand
{
	ENABLE = 1,		// Transition all devices to operation enabled
	DISABLE = 2,	// Command disable voltage to all devices
	EMERGENCY_STOP = 3,		// Quick stop all devices
	REARM = 4		// Enable all devices after an error or quickstop
};
/*! \endcond */

}

#endif
