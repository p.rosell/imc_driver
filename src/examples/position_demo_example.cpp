#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <math.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "position_demo");
	ros::NodeHandle nh = ros::NodeHandle();

	ros::Publisher position_publisher;
	position_publisher = nh.advertise<std_msgs::Float64MultiArray>("/motors/joint_position_streamer/command", 1, false);
	ros::Duration(1.0).sleep();
	std_msgs::Float64MultiArray command;
	command.data.resize(2);
	std::vector<double> command_one;
	std::vector<double> command_two;
	command_one.push_back(M_PI);
	command_two.push_back(M_PI);
	command_one.push_back(M_PI-M_PI/8);
	command_two.push_back(M_PI-M_PI/8);
	command_one.push_back(M_PI);
	command_two.push_back(M_PI);
	command_one.push_back(M_PI+M_PI/8);
	command_two.push_back(M_PI+M_PI/8);
	command_one.push_back(M_PI);
	command_two.push_back(M_PI);
	command_one.push_back(M_PI-M_PI/6);
	command_two.push_back(M_PI+M_PI/6);
	command_one.push_back(M_PI);
	command_two.push_back(M_PI);
	command_one.push_back(M_PI+M_PI/6);
	command_two.push_back(M_PI-M_PI/6);
	command_one.push_back(M_PI);
	command_two.push_back(M_PI);

	command_one.push_back(M_PI+M_PI/16);
	command_two.push_back(M_PI);

	command_one.push_back(M_PI+M_PI/16);
	command_two.push_back(M_PI-M_PI/16);

	command_one.push_back(M_PI);
	command_two.push_back(M_PI-M_PI/16);

	command_one.push_back(M_PI);
	command_two.push_back(M_PI);

	command_one.push_back(M_PI-M_PI/16);
	command_two.push_back(M_PI);

	command_one.push_back(M_PI-M_PI/16);
	command_two.push_back(M_PI+M_PI/16);

	command_one.push_back(M_PI);
	command_two.push_back(M_PI+M_PI/16);

	command_one.push_back(M_PI);
	command_two.push_back(M_PI);

	command_one.push_back(3*M_PI);
	command_two.push_back(3*M_PI);

	command.data.at(0) = command_one.at(0);
	command.data.at(1) = command_two.at(0);
	position_publisher.publish(command);
	ros::Duration(2.0).sleep();

	for (int i = 0; i < command_one.size(); i++)
	{
		command.data.at(0) = command_one.at(i);
		command.data.at(1) = command_two.at(i);
		position_publisher.publish(command);
		ros::Duration(0.5).sleep();
	}

	return 0;
}
