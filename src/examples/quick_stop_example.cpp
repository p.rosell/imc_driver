#include "imc_driver/multi_device.h"

int main(int argc, char *argv[])
{
	char * ifname = argv[1];
	int num_devices = std::stoi(argv[2]);
	double timeout = 10.0; // seconds

	imc_driver::MultiDevice devices;

	// set verbosity to see outputs on terminal
	devices.setVerbose(true);

	// init devices
	devices.init(ifname, num_devices, timeout);

	// set max torque (needed to allow motion)
	devices.setMaxAllowedTorque(500); // per mil of rated torque

	// configure other parameters
	devices.setProfileAcceleration(600.0); // rad/s^2
	devices.setProfileDeceleration(600.0);
	devices.setMaxAcceleration(600.0);
	devices.setMaxDeceleration(600.0);
	devices.setMaxProfileVelocity(314.0); // rad/s

	// enable with torque mode
	devices.setOperationMode(imc_driver::OperationMode::PROFILE_TORQUE);
	devices.activate(timeout);

	// check quick stop option code
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Quick stop option code at device " << i << ": " << devices.getQuickStopOptionCode(i) << std::endl;


	// send torque command and quick stop when velocity higher than threshold
	std::vector<double> torque_command(num_devices, 0.1); // Nm
	double velocity_threshold = 100.0*M_PI; // rad/s
	bool overspeed = false;
	std::chrono::time_point<std::chrono::system_clock> start_time = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time = std::chrono::system_clock::now() - start_time;
	while ( elapsed_time.count() <= 5.0 )
	{
		// update data from devices
		devices.read();

		// check if overspeed in some device (check only if not overspeeded already)
		if ( !overspeed )
		{
			for (unsigned int i = 0; i < num_devices; i++ )
			{
				double velocity = devices.getActualVelocity(i);
				if ( velocity > velocity_threshold )
				{
					std::cout << "Actual velocity at device " << i << ": " << velocity << std::endl;
					overspeed = true;
					break;
				}
			}
		}

		// send command only if not overspeeding, otherwise quick stop
		if ( overspeed )
			devices.quickStop();
		else
			devices.setTargetTorque(torque_command);

		devices.write();

		usleep(10000);
		elapsed_time = std::chrono::system_clock::now() - start_time;
	}

	devices.deactivate(timeout);
	devices.close();

	return 0;
}
