#include "imc_driver/multi_device.h"

// keyboard hit detection
#include <sys/ioctl.h>
#include <termios.h>

// txt file generation
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/stat.h>

char _kbhit() {
    static const int STDIN = 0;
    static bool initialized = false;
    char key;

    if (! initialized) {
        // Use termios to turn off line buffering
        termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }
    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    if(bytesWaiting)
        key = getchar();

    return key;
}

std::string time_stamp()
{
   const auto now = std::time(nullptr) ;
   char cstr[256] {};
   return std::strftime( cstr, sizeof(cstr), "%Y%m%d_%H%M%S", std::localtime(&now) ) ? cstr : "" ;
}

int main(int argc, char *argv[])
{
    char *ifname = argv[1];
    int num_devices = std::stoi(argv[2]);
    int mode = std::stoi(argv[3]);
    double timeout = 10.0; // seconds
    double loop_duration = 5.0; // duration of each testing loop [s]
    unsigned int loop_period = 1000; // loop period (inverse of r/w rate) [ms]
    char log_dir[256]; // buffer to allocate the current working directory

    std::string profile_id; // string containing the motor1 profile id
    std::cout << std::endl << "STARTING EXAMPLE" << std::endl;

    imc_driver::MultiDevice devices;

    // set verbosity to see outputs on terminal
    devices.setVerbose(true);

    // init devices
    devices.init(ifname, num_devices, timeout);

    // set max torque (needed to allow motion)
    devices.setMaxAllowedTorque(500); // per mil of rated torque

    // configure other parameters
    devices.setProfileAcceleration(600.0); // rad/s^2
    devices.setProfileDeceleration(600.0);
    devices.setMaxAcceleration(600.0);
    devices.setMaxDeceleration(600.0);
    devices.setMaxProfileVelocity(10*M_PI); // rad/s

    // All devices transition to OPERATION_ENABLED
    devices.activate(timeout);

    // switch to torque mode for motor 0: send torque commands for a while and stop
    devices.setOperationMode(imc_driver::OperationMode::PROFILE_TORQUE, 0);
    std::vector<double> motor0_command(num_devices, 0.0); // Nm/s
    std::vector<double> motor1_command(num_devices, 0.0);
    double motor0_prevPos = 0.0;

    // choose motor tracking mode: 0 absolute position, 1: relative position, 2: velocity
    switch (mode)
    {
    case 0:
        // define profile
        profile_id = "abspos";

        //Define model-specific tuning parameters
        loop_period = 1500;
        devices.setProfileAcceleration(391.0); // rad/s^2
        devices.setProfileDeceleration(391.0); // rad/s^2
        devices.setMaxProfileVelocity(10*M_PI); // rad/s

        std::cout << std::endl << "SYNC_MOTORS EXAMPLE POSITION (ABSOLUTE)" << std::endl;
        devices.setOperationMode(imc_driver::OperationMode::PROFILE_POSITION, 1);
        devices.setTargetPositionRelative(false); // the position command will be absolute with respect to the power-up position
        devices.setPositionChangeSetImmediately(true); // the drive will start to move to the next position, even if the current one was not reached
        break;
    case 1:
        // define profile
        profile_id = "relpos";

        //Define model-specific tuning parameters
        loop_period = 2000;
        devices.setProfileAcceleration(600.0);  // rad/s^2
        devices.setProfileDeceleration(600.0);  // rad/s^2
        devices.setMaxProfileVelocity(8*M_PI); // rad/s

        std::cout << std::endl << "SYNC_MOTORS EXAMPLE POSITION (RELATIVE)" << std::endl;
        devices.setOperationMode(imc_driver::OperationMode::PROFILE_POSITION, 1);
        devices.setTargetPositionRelative(true); // relative
        devices.setPositionChangeSetImmediately(true); // the drive will start to move to the next position, even if the current one was not reached
        devices.read();
        motor0_prevPos = std::abs(devices.getActualPosition(0)); // initialise offset value at the motor 0 position
        break;
    case 2:
        // define profile
        profile_id = "velpos";

        std::cout << std::endl << "SYNC_MOTORS EXAMPLE VELOCITY" << std::endl;
        devices.setOperationMode(imc_driver::OperationMode::PROFILE_VELOCITY, 1);
        break;
    }

    /* Create position log folder and file*/
    // Obtain local directory
    if(!getcwd(log_dir, sizeof(log_dir)))
        perror("local directory not found");
    // Obtain time stamp
    std::string time_stmp = time_stamp();
    std::string log_id = profile_id + "_log.mat";
    // Obtain folder name
    std::string log_dir2(log_dir);
    log_dir2 = log_dir2 + "/" + time_stmp.substr(0,time_stmp.find('_'));
    // Create log folder
    std::cout << "log folder created in: " << log_dir2 << std::endl;
    mkdir(log_dir2.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    // Create log file
    std::string log_total = log_dir2 + "/" + log_id;
    std::ofstream myFile(log_total.c_str());
    // write time stamp at the start of the file
    myFile<< time_stmp << "\n";

    // time variables
    std::chrono::time_point<std::chrono::system_clock> start_time = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = std::chrono::system_clock::now() - start_time;
    do
    {
        devices.read();
        devices.setTargetTorque(motor0_command);

        if (mode == 0)
        {
            motor1_command.at(1) = devices.getActualPosition(0);
            devices.setTargetPosition(motor1_command);
        }
        else if ( mode == 1)
        {
            double motor0_currPos = std::abs(devices.getActualPosition(0));
            double offset = motor0_prevPos - motor0_currPos;
            motor0_prevPos = motor0_currPos;

            if (std::abs(offset) >= 0.001)
            {
                motor1_command.at(1) = offset;
                devices.latchRelativePositionTarget();
                devices.setTargetPosition(motor1_command);
            }
        }
        else if ( mode == 2 )
        {
            motor1_command.at(1) = devices.getActualVelocity(0);
            devices.setTargetVelocity(motor1_command);
        }

        devices.write();

        elapsed_time = std::chrono::system_clock::now() - start_time;
        // write log file
        myFile<<std::chrono::duration_cast<std::chrono::microseconds>(elapsed_time).count() << "; ";
        myFile<<devices.getActualPosition(0) << "; " <<devices.getActualPosition(1)<< "\n";

        usleep(loop_period);
    }
    while (_kbhit() != 'q');

    // close log file
    myFile.close();

    devices.read();	// update last info
    for (unsigned int i = 0; i < num_devices; i++ )
    {
        std::cout << "Actual torque at device " << i << ": " << devices.getActualTorque(i) << std::endl;
        std::cout << "Actual position at device " << i << ": " << devices.getActualPosition(i) << std::endl;
        std::cout << "Actual velocity at device " << i << ": " << devices.getActualVelocity(i) << std::endl;
    }

    // check state machine state
    std::cout << std::endl << "CLOSING EXAMPLE" << std::endl;
    for (unsigned int i = 0; i < num_devices; i++ )
        std::cout << "Device " << i << " state: " << devices.getStateMachineState(i) << std::endl;

    devices.deactivate(timeout);
    devices.close();

    return 0;
}
