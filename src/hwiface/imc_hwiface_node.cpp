#include <imc_hwiface/imc_hwiface.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "imc_hwiface_node");
	ros::start();
	ros::NodeHandle nh = ros::NodeHandle();

	ros::AsyncSpinner spinner(1);
	spinner.start();

	double rate;
	if( !nh.getParam("rate_in_hz", rate) )
	{
		ROS_WARN("Couldn't read parameter rate_in_hz, setting 100Hz by default");
		rate = 100;
	}

	imc_driver::IMCHWIface imc_hwiface;

	if(!imc_hwiface.init())
	{
		ROS_ERROR("IMC hardware interface not inialized, abort");
		return -1;
	}

	controller_manager::ControllerManager manager(&imc_hwiface, nh);

	ros::Time prev_time = ros::Time::now();

	ros::Rate loop_rate(rate);

	while( ros::ok() )
	{
		ros::Time time = ros::Time::now();
		ros::Duration elapsed_time = time - prev_time;
		prev_time = time;

		imc_hwiface.read(elapsed_time);
		manager.update(time, elapsed_time, imc_hwiface.resetControllersRequired());
		imc_hwiface.write(elapsed_time);

		loop_rate.sleep();
	}

	ROS_INFO_STREAM("Shutting down imc hwifaces" );
	spinner.stop();
	return 0;
}
