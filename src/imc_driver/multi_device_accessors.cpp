#include <imc_driver/multi_device.h>

namespace imc_driver
{

bool MultiDevice::allTargetsReached()
{
	for (unsigned int i = 0; i < ec_slavecount; i++)
		if ( !target_reached__.at(i) )
			return false;
	return true;
}

bool  MultiDevice::allSetPointAcknowledged()
{
	for (unsigned int i = 0; i < ec_slavecount; i++)
		if ( !set_point_acknowledge__.at(i) )
			return false;
	return true;
}

MultiDeviceState MultiDevice::getMultiDeviceState()
{
	return multi_device_state__;
}

OperationMode MultiDevice::getOperationMode(const unsigned int & __device_number)
{
	return current_operation_mode__.at(__device_number);
}

std::vector<OperationMode> MultiDevice::getOperationMode()
{
	return current_operation_mode__;
}

StateMachineState MultiDevice::getStateMachineState(const unsigned int & __device_number)
{
	return state_machine_state__.at(__device_number);
}

std::vector<StateMachineState> MultiDevice::getStateMachineState()
{
	return state_machine_state__;
}

std::string MultiDevice::getStateMachineStateAsString(const unsigned int & __device_number)
{
	return StateMachineState2String(state_machine_state__.at(__device_number));
}

std::vector<std::string> MultiDevice::getStateMachineStateAsString()
{
	std::vector<std::string> state_strings(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		state_strings.at(i) = getStateMachineStateAsString(i);
	return state_strings;
}

uint16 MultiDevice::getEthercatState(const unsigned int & __device_number)
{
	return ethercat_state__.at(__device_number);
}

std::vector<uint16> MultiDevice::getEthercatState()
{
	return ethercat_state__;
}

double MultiDevice::getRatedTorque(const unsigned int & __device_number)
{
	return rated_torque__.at(__device_number);
}

std::vector<double> MultiDevice::getRatedTorque()
{
	return rated_torque__;
}

double MultiDevice::getActualVelocity(const unsigned int & __device_number)
{
	return velocity_feedback__.at(__device_number);
}

std::vector<double> MultiDevice::getActualVelocity()
{
	return velocity_feedback__;
}

double MultiDevice::getActualTorque(const unsigned int & __device_number)
{
	return torque_feedback__.at(__device_number);
}

std::vector<double> MultiDevice::getActualTorque()
{
	return torque_feedback__;
}

double MultiDevice::getActualPosition(const unsigned int & __device_number)
{
	return position_feedback__.at(__device_number);
}

std::vector<double> MultiDevice::getActualPosition()
{
	return position_feedback__;
}

double MultiDevice::getActualFollowingError(const unsigned int & __device_number)
{
	return following_error__.at(__device_number);
}

std::vector<double> MultiDevice::getActualFollowingError()
{
	return following_error__;
}

double MultiDevice::getDriveTemperature(const unsigned int & __device_number)
{
	return drive_temperature__.at(__device_number);
}

std::vector<double> MultiDevice::getDriveTemperature()
{
	return drive_temperature__;
}

std::vector<OperationMode> MultiDevice::getSupportedDriveModes(const unsigned int & __device_number)
{
	uint32 modes_value;
	readSDO<uint32>(__device_number, SUPPORTED_DRIVE_MODES_IDX, SUPPORTED_DRIVE_MODES_SUB,  &modes_value);

	std::vector<OperationMode> supported_drive_modes;
	if ( (modes_value & 0x00000001) == 0x00000001 )
		supported_drive_modes.push_back(OperationMode::PROFILE_POSITION);
	if ( (modes_value & 0x00000002) == 0x00000002 )
		supported_drive_modes.push_back(OperationMode::VELOCITY);
	if ( (modes_value & 0x00000004) == 0x00000004 )
		supported_drive_modes.push_back(OperationMode::PROFILE_VELOCITY);
	if ( (modes_value & 0x00000008) == 0x00000008 )
		supported_drive_modes.push_back(OperationMode::PROFILE_TORQUE);
	if ( (modes_value & 0x00000020) == 0x00000020 )
		supported_drive_modes.push_back(OperationMode::HOMING);
	if ( (modes_value & 0x00000040) == 0x00000040 )
		supported_drive_modes.push_back(OperationMode::INTERPOLATED_POSITION);
	if ( (modes_value & 0x00000080) == 0x00000080 )
		supported_drive_modes.push_back(OperationMode::CYCLIC_SYNC_POSITION);
	if ( (modes_value & 0x00000100) == 0x00000100 )
		supported_drive_modes.push_back(OperationMode::CYCLIC_SYNC_VELOCITY);
	if ( (modes_value & 0x00000200) == 0x00000200 )
		supported_drive_modes.push_back(OperationMode::CYCLIC_SYNC_TORQUE);
	if ( (modes_value & 0x00010000) == 0x00010000 )
		supported_drive_modes.push_back(OperationMode::OPEN_LOOP_SCALAR);
	if ( (modes_value & 0x00020000) == 0x00020000 )
		supported_drive_modes.push_back(OperationMode::OPEN_LOOP_VECTOR);

	return supported_drive_modes;
}

std::vector<std::vector<OperationMode>> MultiDevice::getSupportedDriveModes()
{
	std::vector<std::vector<OperationMode>> all_devices_modes(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		all_devices_modes.at(i) = getSupportedDriveModes(i);
	return all_devices_modes;
}

double MultiDevice::getProfileAcceleration(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, PROFILE_ACCELERATION_IDX, PROFILE_ACCELERATION_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getProfileAcceleration()
{
	std::vector<double> profile_accelerations(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		profile_accelerations.at(i) = getProfileAcceleration(i);
	return profile_accelerations;
}

double MultiDevice::getProfileDeceleration(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, PROFILE_DECELERATION_IDX, PROFILE_DECELERATION_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getProfileDeceleration()
{
	std::vector<double> profile_decelerations(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		profile_decelerations.at(i) = getProfileDeceleration(i);
	return profile_decelerations;
}

double MultiDevice::getProfileVelocity(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, PROFILE_VELOCITY_IDX, PROFILE_VELOCITY_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getProfileVelocity()
{
	std::vector<double> velocities(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		velocities.at(i) = getProfileVelocity(i);
	return velocities;
}

double MultiDevice::getMaxProfileVelocity(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, MAX_PROFILE_VELOCITY_IDX, MAX_PROFILE_VELOCITY_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getMaxProfileVelocity()
{
	std::vector<double> velocities(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		velocities.at(i) = getMaxProfileVelocity(i);
	return velocities;
}

double MultiDevice::getMaxAcceleration(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, MAX_ACCELERATION_IDX, MAX_ACCELERATION_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getMaxAcceleration()
{
	std::vector<double> accelerations(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		accelerations.at(i) = getMaxAcceleration(i);
	return accelerations;
}

double MultiDevice::getMaxDeceleration(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, MAX_DECELERATION_IDX, MAX_DECELERATION_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getMaxDeceleration()
{
	std::vector<double> decelerations(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		decelerations.at(i) = getMaxDeceleration(i);
	return decelerations;
}

std::string MultiDevice::getDeviceName(const unsigned int & __device_number)
{
	return std::string(ec_slave[__device_number+1].name);
}

std::vector<std::string> MultiDevice::getDeviceName()
{
	std::vector<std::string> names(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		names.at(i) = getDeviceName(i);
	return names;
}

std::string MultiDevice::getDriveName(const unsigned int & __device_number)
{
	uint64 value;
	readSDO<uint64>(__device_number, DRIVE_NAME_IDX, DRIVE_NAME_SUB,  &value);
	char char_array[sizeof(value)];
	std::memcpy(char_array, &value, sizeof(value));
	return std::string(char_array);
}

std::vector<std::string> MultiDevice::getDriveName()
{
	std::vector<std::string> names(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		names.at(i) = getDriveName(i);
	return names;
}

double MultiDevice::getPositionControlProportionalConstant(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, POSITION_CONTROL_KP_IDX, POSITION_CONTROL_KP_SUB, rated_torque__.at(__device_number) * encoder_increments_per_turn__.at(__device_number) / (1000.0 * 2.0 * M_PI)); // to Nm/rad
}

std::vector<double> MultiDevice::getPositionControlProportionalConstant()
{
	std::vector<double> proportional_constants(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		proportional_constants.at(i) = getPositionControlProportionalConstant(i);
	return proportional_constants;
}

double MultiDevice::getPositionControlIntegralConstant(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, POSITION_CONTROL_KI_IDX, POSITION_CONTROL_KI_SUB, rated_torque__.at(__device_number) * encoder_increments_per_turn__.at(__device_number) / (1000.0 * 2.0 * M_PI)); // to Nm/(rad*s)
}

std::vector<double> MultiDevice::getPositionControlIntegralConstant()
{
	std::vector<double> integral_constants(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		integral_constants.at(i) = getPositionControlIntegralConstant(i);
	return integral_constants;
}

double MultiDevice::getPositionControlDerivativeConstant(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, POSITION_CONTROL_KD_IDX, POSITION_CONTROL_KD_SUB, rated_torque__.at(__device_number) * encoder_increments_per_turn__.at(__device_number) / (1000.0 * 2.0 * M_PI)); // to Nm*s/rad
}

std::vector<double> MultiDevice::getPositionControlDerivativeConstant()
{
	std::vector<double> derivative_constants(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		derivative_constants.at(i) = getPositionControlDerivativeConstant(i);
	return derivative_constants;
}

double MultiDevice::getVelocityControlProportionalConstant(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, VELOCITY_CONTROL_KP_IDX, VELOCITY_CONTROL_KP_SUB, rated_torque__.at(__device_number) * encoder_increments_per_turn__.at(__device_number) / (1000.0 * 2.0 * M_PI)); // to Nm/(rad*s)
}

std::vector<double> MultiDevice::getVelocityControlProportionalConstant()
{
	std::vector<double> proportional_constants(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		proportional_constants.at(i) = getVelocityControlProportionalConstant(i);
	return proportional_constants;
}

double MultiDevice::getVelocityControlIntegralConstant(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, VELOCITY_CONTROL_KI_IDX, VELOCITY_CONTROL_KI_SUB, rated_torque__.at(__device_number) * encoder_increments_per_turn__.at(__device_number) / (1000.0 * 2.0 * M_PI)); // to Nm/(rad*s^2)
}

std::vector<double> MultiDevice::getVelocityControlIntegralConstant()
{
	std::vector<double> integral_constants(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		integral_constants.at(i) = getVelocityControlIntegralConstant(i);
	return integral_constants;
}

double MultiDevice::getVelocityControlDerivativeConstant(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, VELOCITY_CONTROL_KD_IDX, VELOCITY_CONTROL_KD_SUB, rated_torque__.at(__device_number) * encoder_increments_per_turn__.at(__device_number) / (1000.0 * 2.0 * M_PI)); // to Nm/rad
}

std::vector<double> MultiDevice::getVelocityControlDerivativeConstant()
{
	std::vector<double> derivative_constants(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		derivative_constants.at(i) = getVelocityControlDerivativeConstant(i);
	return derivative_constants;
}

double MultiDevice::getMaxCurrent(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint16>(__device_number, MAX_CURRENT_IDX, MAX_CURRENT_SUB, rated_current__.at(__device_number) / 1000.0); // to A
}

std::vector<double> MultiDevice::getMaxCurrent()
{
	std::vector<double> max_currents(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		max_currents.at(i) = getMaxCurrent(i);
	return max_currents;
}

double MultiDevice::getMaxMotorSpeed(const unsigned int & __device_number)
{
	return getValueFromDevice<double,uint32>(__device_number, MAX_MOTOR_SPEED_IDX, MAX_MOTOR_SPEED_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getMaxMotorSpeed()
{
	std::vector<double> max_motor_speeds(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		max_motor_speeds.at(i) = getMaxMotorSpeed(i);
	return max_motor_speeds;
}

uint8 MultiDevice::getErrorRegister(const unsigned int & __device_number)
{
	uint8 error_register;
	readSDO<uint8>(__device_number, ERROR_REGISTER_IDX, ERROR_REGISTER_SUB,  &error_register);
	return error_register;
}

std::vector<uint8> MultiDevice::getErrorRegister()
{
	std::vector<uint8> error_registers(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		error_registers.at(i) = getErrorRegister(i);
	return error_registers;
}

uint8 MultiDevice::getHistoryNumberOfErrors(const unsigned int & __device_number)
{
	uint8 value;
	readSDO<uint8>(__device_number, HISTORY_NUMBER_OF_ERRORS_IDX, HISTORY_NUMBER_OF_ERRORS_SUB,  &value);
	return value;
}

std::vector<uint8> MultiDevice::getHistoryNumberOfErrors()
{
	std::vector<uint8> numbers_of_errors(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		numbers_of_errors.at(i) = getErrorRegister(i);
	return numbers_of_errors;
}

std::vector<uint32> MultiDevice::getHistoryErrors(const unsigned int & __device_number)
{
	uint8 num_errors = getHistoryNumberOfErrors(__device_number);
	std::vector<uint32> errors;
	uint32 value;
	for ( uint8 i = 0; i < num_errors; i++)
	{
		uint8 subindex = HISTORY_ERRORS_SUB + i;
		readSDO<uint32>(__device_number, HISTORY_ERRORS_IDX, subindex,  &value);
		errors.push_back(value);
	}
	return errors;
}

std::vector<std::vector<uint32>> MultiDevice::getHistoryErrors()
{
	std::vector<std::vector<uint32>> history_errors(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		history_errors.at(i) = getHistoryErrors(i);
	return history_errors;
}

uint16 MultiDevice::getLastErrorCode(const unsigned int & __device_number)
{
	return error_code__.at(__device_number);
}

std::vector<uint16> MultiDevice::getLastErrorCode()
{
	std::vector<uint16> error_codes(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		error_codes.at(i) = getLastErrorCode(i);
	return error_codes;
}

std::string MultiDevice::getLastErrorAsString(const unsigned int & __device_number)
{
	return errorCode2String(error_code__.at(__device_number));
}

std::vector<std::string> MultiDevice::getLastErrorAsString()
{
	std::vector<std::string> error_strings(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		error_strings.at(i) = getLastErrorAsString(i);
	return error_strings;
}

uint32 MultiDevice::getIdentityVendorID(const unsigned int & __device_number)
{
	uint32 vendor_id;
	readSDO<uint32>(__device_number, IDENTITY_VENDOR_ID_IDX, IDENTITY_VENDOR_ID_SUB, &vendor_id);
	return vendor_id;
}

std::vector<uint32> MultiDevice::getIdentityVendorID()
{
	std::vector<uint32> vendorIDs(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		vendorIDs.at(i) = getIdentityVendorID(i);
	return vendorIDs;
}

uint32 MultiDevice::getIdentityProductCode(const unsigned int & __device_number)
{
	uint32 product_code;
	readSDO<uint32>(__device_number, IDENTITY_PRODUCT_CODE_IDX, IDENTITY_PRODUCT_CODE_SUB, &product_code);
	return product_code;
}

std::vector<uint32> MultiDevice::getIdentityProductCode()
{
	std::vector<uint32> product_codes(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		product_codes.at(i) = getIdentityProductCode(i);
	return product_codes;
}

uint32 MultiDevice::getIdentityRevisionNumber(const unsigned int & __device_number)
{
	uint32 revision_number;
	readSDO<uint32>(__device_number, IDENTITY_REVISION_NUMBER_IDX, IDENTITY_REVISION_NUMBER_SUB, &revision_number);
	return revision_number;
}

std::vector<uint32> MultiDevice::getIdentityRevisionNumber()
{
	std::vector<uint32> revision_numbers(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		revision_numbers.at(i) = getIdentityRevisionNumber(i);
	return revision_numbers;
}

uint32 MultiDevice::getIdentitySerialNumber(const unsigned int & __device_number)
{
	uint32 serial_number;
	readSDO<uint32>(__device_number, IDENTITY_SERIAL_NUMBER_IDX, IDENTITY_SERIAL_NUMBER_SUB, &serial_number);
	return serial_number;
}

std::vector<uint32> MultiDevice::getIdentitySerialNumber()
{
	std::vector<uint32> serial_numbers(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		serial_numbers.at(i) = getIdentitySerialNumber(i);
	return serial_numbers;
}

uint16 MultiDevice::getMotorType(const unsigned int & __device_number)
{
	uint16 motor_type;
	readSDO<uint16>(__device_number, MOTOR_TYPE_IDX, MOTOR_TYPE_SUB, &motor_type);
	return motor_type;
}

std::vector<uint16> MultiDevice::getMotorType()
{
	std::vector<uint16> motor_types(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		motor_types.at(i) = getMotorType(i);
	return motor_types;
}

int16 MultiDevice::getQuickStopOptionCode(const unsigned int & __device_number)
{
	int16 quick_stop_option_code;
	readSDO<int16>(__device_number, QUICK_STOP_OPTION_IDX, QUICK_STOP_OPTION_SUB, &quick_stop_option_code);
	return quick_stop_option_code;
}

std::vector<int16> MultiDevice::getQuickStopOptionCode()
{
	std::vector<int16> option_codes(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		option_codes.at(i) = getQuickStopOptionCode(i);
	return option_codes;
}

double MultiDevice::getPositionDemandValue(const unsigned int & __device_number)
{
	return getValueFromDevice<double,int32>(__device_number, POSITION_DEMAND_VALUE_IDX, POSITION_DEMAND_VALUE_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getPositionDemandValue()
{
	std::vector<double> position_demand_values(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		position_demand_values.at(i) = getPositionDemandValue(i);
	return position_demand_values;
}

uint16 MultiDevice::getPositioningRelativeOption(const unsigned int & __device_number)
{
	uint16 option;
	readSDO<uint16>(__device_number, POSITIONING_RELATIVE_OPTION_IDX, POSITIONING_RELATIVE_OPTION_SUB, &option);
	return (option & (~0xFFFC)); // just keep bits 0 and 1
}

std::vector<uint16> MultiDevice::getPositioningRelativeOption()
{
	std::vector<uint16> positioning_relative_options(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		positioning_relative_options.at(i) = getPositioningRelativeOption(i);
	return positioning_relative_options;
}

uint16 MultiDevice::getPositioningRotaryAxisOption(const unsigned int & __device_number)
{
	uint16 option;
	readSDO<uint16>(__device_number, POSITIONING_ROTARY_AXIS_OPTION_IDX, POSITIONING_ROTARY_AXIS_OPTION_SUB, &option);
	return (option & (~0xFF3F)); // just keep bits 6 and 7
}

std::vector<uint16> MultiDevice::getPositioningRotaryAxisOption()
{
	std::vector<uint16> positioning_rotary_axis_options(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++)
		positioning_rotary_axis_options.at(i) = getPositioningRotaryAxisOption(i);
	return positioning_rotary_axis_options;
}

double MultiDevice::getMaxPositionRangeLimit(const unsigned int & __device_number)
{
	return getValueFromDevice<double,int32>(__device_number, MAX_POSITION_RANGE_LIMIT_IDX, MAX_POSITION_RANGE_LIMIT_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getMaxPositionRangeLimit()
{
	std::vector<double> max_limits(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		max_limits.at(i) = getMaxPositionRangeLimit(i);
	return max_limits;
}

double MultiDevice::getMinPositionRangeLimit(const unsigned int & __device_number)
{
	return getValueFromDevice<double,int32>(__device_number, MIN_POSITION_RANGE_LIMIT_IDX, MIN_POSITION_RANGE_LIMIT_SUB, 2.0 * M_PI / encoder_increments_per_turn__.at(__device_number));
}

std::vector<double> MultiDevice::getMinPositionRangeLimit()
{
	std::vector<double> min_limits(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		min_limits.at(i) = getMinPositionRangeLimit(i);
	return min_limits;
}

std::string MultiDevice::getManufacturerDeviceName(const unsigned int & __device_number)
{
	return getStringFromDevice(__device_number, MANUFACTURER_DEVICE_NAME_IDX, MANUFACTURER_DEVICE_NAME_SUB);
}

std::vector<std::string> MultiDevice::getManufacturerDeviceName()
{
	std::vector<std::string> manufacturer_device_names(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		manufacturer_device_names.at(i) = getManufacturerDeviceName(i);
	return manufacturer_device_names;
}

std::string MultiDevice::getManufacturerHardwareName(const unsigned int & __device_number)
{
	return getStringFromDevice(__device_number, MANUFACTURER_HARDWARE_NAME_IDX, MANUFACTURER_HARDWARE_NAME_SUB);
}

std::vector<std::string> MultiDevice::getManufacturerHardwareName()
{
	std::vector<std::string> manufacturer_hardware_names(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		manufacturer_hardware_names.at(i) = getManufacturerHardwareName(i);
	return manufacturer_hardware_names;
}

std::string MultiDevice::getSoftwareVersion(const unsigned int & __device_number)
{
	return getStringFromDevice(__device_number, SOFTWARE_VERSION_IDX, SOFTWARE_VERSION_SUB);
}

std::vector<std::string> MultiDevice::getSoftwareVersion()
{
	std::vector<std::string> software_versions(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		software_versions.at(i) = getSoftwareVersion(i);
	return software_versions;
}

std::string MultiDevice::getHTTPDriveCatalogAddress(const unsigned int & __device_number)
{
	return getStringFromDevice(__device_number, HTTP_DRIVE_CATALOG_ADDRESS_IDX, HTTP_DRIVE_CATALOG_ADDRESS_SUB);
}

std::vector<std::string> MultiDevice::getHTTPDriveCatalogAddress()
{
	std::vector<std::string> catalog_addresses(ec_slavecount);
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		catalog_addresses.at(i) = getHTTPDriveCatalogAddress(i);
	return catalog_addresses;
}

}
