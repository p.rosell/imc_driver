#include <imc_driver/multi_device.h>

namespace imc_driver
{
	std::string MultiDevice::errorCode2String(uint16 & __error_code)
	{
		switch ( __error_code )
		{
			case 0x0000:
				return "No Error";
				break;
			case 0x2280:
				return "Over-current peak has been detected in phase or DC-Bus line (HW system protection)";
				break;
			case 0x2290:
				return "Over-current peak has been detected in phase (FW system protection)";
				break;
			case 0x2291:
				return "Over-current peak has been detected in phase A (FW system protection)";
				break;
			case 0x2292:
				return "Over-current peak has been detected in phase B (FW system protection)";
				break;
			case 0x2293:
				return "Over-current peak has been detected in phase C (FW system protection)";
				break;
			case 0x22A0:
				return "Initial current reading out of range (FW system protection)";
				break;
			case 0x22A1:
				return "Initial current reading of Phase A out of range (FW system protection)";
				break;
			case 0x22A2:
				return "Initial current reading of Phase B out of range (FW system protection)";
				break;
			case 0x22A3:
				return "Initial current reading of Phase C out of range (FW system protection)";
				break;
			case 0x2350:
				return "An I2T over-current has been detected (FW system protection)";
				break;
			case 0x2380:
				return "Saturation of current measurement system has been detected";
				break;
			case 0x2381:
				return "Saturation of current measurement system has been detected in phase A";
				break;
			case 0x2382:
				return "Saturation of current measurement system has been detected in phase B";
				break;
			case 0x2383:
				return "Saturation of current measurement system has been detected in phase C";
				break;
			case 0x3210:
				return "System over voltage detected";
				break;
			case 0x3211:
				return "User over voltage detected";
				break;
			case 0x3220:
				return "System under voltage detected";
				break;
			case 0x3221:
				return "User under voltage detected";
				break;
			case 0x4300:
				return "User temperature out of range detected";
				break;
			case 0x4310:
				return "System over temperature detected (FW system protection)";
				break;
			case 0x4320:
				return "System under temperature detected (FW system protection)";
				break;
			case 0x5210:
				return "Internal VGA communication problem detected";
				break;
			case 0x5400:
				return "Output power section problem detected (system protection)";
				break;
			case 0x5430:
				return "Input stage problem detected. Voltage not stable or not available (system protection)";
				break;
			case 0x5530:
				return "Internal NVM communication problem detected";
				break;
			case 0x6185:
				return "Internal EEPROM full error";
				break;
			case 0x6186:
				return "Internal EEPROM full error (Communication Dictionary)";
				break;
			case 0x6187:
				return "Internal EEPROM full error (Manufacturer Dictionary)";
				break;
			case 0x6188:
				return "Internal EEPROM full error (Device Dictionary)";
				break;
			case 0x7121:
				return "Motor blocked";
				break;
			case 0x7124:
				return "Motor not detected";
				break;
			case 0x7303:
				return "Error in resolver signals detected";
				break;
			case 0x7306:
				return "Differential encoder broken wire detected";
				break;
			case 0x7380:
				return "SSI encoder error";
				break;
			case 0x7381:
				return "SinCos encoder error";
				break;
			case 0x8110:
				return "CAN bus over-run";
				break;
			case 0x8120:
				return "CAN error in passive mode";
				break;
			case 0x8130:
				return "Lifeguard error";
				break;
			case 0x8140:
				return "Recovered from CAN bus off";
				break;
			case 0x8141:
				return "CAN Bus off occurred";
				break;
			case 0x8210:
				return "PDO not processed due to length error";
				break;
			case 0x8280:
				return "Error decoding serial message";
				break;
			case 0x8613:
				return "Homing timeout detected";
				break;
			case 0xFF02:
				return "Not allowed digital hall combination detected";
				break;
			case 0xFF03:
				return "Not allowed sequence of digital halls has been detected";
				break;
			case 0xFF04:
				return "Angular error in forced alignment method is out of tolerance";
				break;
			case 0xFF05:
				return "Interpolated position mode buffer full";
				break;
			case 0xFF06:
				return "Error in Analog hall signals detected";
				break;
			case 0xFF10:
				return "A stand-alone divide by zero instruction detected";
				break;
			case 0xFF20:
				return "RS232 reception overflow";
				break;
			case 0xFF30:
				return "Executing a non-existing macro or instruction address";
				break;
			case 0xFF31:
				return "Macro stack full";
				break;
			case 0xFF33:
				return "Detected interrupt without associated macro function";
				break;
			case 0xFF34:
				return "Saving or restoring out of learned position space";
				break;
			case 0xFF40:
				return "EtherCAT synchronization error";
				break;
			case 0xFF41:
				return "EtherCAT plugin board disconnected";
				break;
			case 0xFF42:
				return "EtherCAT cable has been disconnected";
				break;
			case 0xFF50:
				return "Incorrect object access";
				break;
			case 0xFF60:
				return "Safe torque off activated";
				break;
			case 0xFF61:
				return "Too many CRC errors in Panasonic encoder";
				break;
			case 0xFF62:
				return "Generic Panasonic encoder error";
				break;
			case 0xFF63:
				return "Communication with Panasonic encoder lost";
				break;
			case 0xFF64:
				return "Panasonic encoder power loss";
				break;
			default:
				return "Unknown error code";
				break;
		}
	}
}
