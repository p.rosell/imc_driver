#include <imc_driver/multi_device.h>

namespace imc_driver
{

void MultiDevice::setVerbose(const bool __verbose)
{
	verbose__ = __verbose;
	thread_verbose_mutex__.lock();
	thread_verbose__ = __verbose;
	thread_verbose_mutex__.unlock();
}

bool MultiDevice::setOperationMode(const OperationMode & __operation_mode, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	desired_operation_mode__.at(__device_number) = __operation_mode;
	return true;
}

bool MultiDevice::setOperationMode(const std::vector<OperationMode> & __operation_modes)
{
	if ( !checkInputSize(__operation_modes.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setOperationMode(__operation_modes.at(i), i);
	return true;
}

void MultiDevice::setOperationMode(const OperationMode & __operation_mode)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setOperationMode(__operation_mode, i);
}

bool MultiDevice::setProfileAcceleration(const double & __profile_acceleration, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __profile_acceleration * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<uint32>(value_in_device_units, __device_number, PROFILE_ACCELERATION_IDX, PROFILE_ACCELERATION_SUB);
	return true;
}

bool MultiDevice::setProfileAcceleration(const std::vector<double> & __profile_accelerations)
{
	if ( !checkInputSize(__profile_accelerations.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setProfileAcceleration(__profile_accelerations.at(i), i);
	return true;
}

void MultiDevice::setProfileAcceleration(const double & __profile_acceleration)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setProfileAcceleration(__profile_acceleration, i);
}

bool MultiDevice::setProfileDeceleration(const double & __profile_deceleration, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __profile_deceleration * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<uint32>(value_in_device_units, __device_number, PROFILE_DECELERATION_IDX, PROFILE_DECELERATION_SUB);
	return true;
}

bool MultiDevice::setProfileDeceleration(const std::vector<double> & __profile_decelerations)
{
	if ( !checkInputSize(__profile_decelerations.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setProfileDeceleration(__profile_decelerations.at(i), i);
	return true;
}

void MultiDevice::setProfileDeceleration(const double & __profile_deceleration)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setProfileDeceleration(__profile_deceleration, i);
}

bool MultiDevice::setProfileVelocity(const double & __profile_velocity, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __profile_velocity * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<uint32>(value_in_device_units, __device_number, PROFILE_VELOCITY_IDX, PROFILE_VELOCITY_SUB);
	return true;
}

bool MultiDevice::setProfileVelocity(const std::vector<double> & __profile_velocities)
{
	if ( !checkInputSize(__profile_velocities.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setProfileVelocity(__profile_velocities.at(i), i);
	return true;
}

void MultiDevice::setProfileVelocity(const double & __profile_velocity)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setProfileVelocity(__profile_velocity, i);
}

bool MultiDevice::setMaxProfileVelocity(const double & __max_profile_velocity, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __max_profile_velocity * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<uint32>(value_in_device_units, __device_number, MAX_PROFILE_VELOCITY_IDX, MAX_PROFILE_VELOCITY_SUB);
	return true;
}

bool MultiDevice::setMaxProfileVelocity(const std::vector<double> & __max_profile_velocities)
{
	if ( !checkInputSize(__max_profile_velocities.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxProfileVelocity(__max_profile_velocities.at(i), i);
	return true;
}

void MultiDevice::setMaxProfileVelocity(const double & __max_velocity)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxProfileVelocity(__max_velocity, i);
}

bool MultiDevice::setMaxAcceleration(const double & __max_acceleration, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __max_acceleration * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<uint32>(value_in_device_units, __device_number, MAX_ACCELERATION_IDX, MAX_ACCELERATION_SUB);
	return true;
}

bool MultiDevice::setMaxAcceleration(const std::vector<double> & __max_accelerations)
{
	if ( !checkInputSize(__max_accelerations.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxAcceleration(__max_accelerations.at(i), i);
	return true;
}

void MultiDevice::setMaxAcceleration(const double & __max_acceleration)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxAcceleration(__max_acceleration, i);
}

bool MultiDevice::setMaxDeceleration(const double & __max_deceleration, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __max_deceleration * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<uint32>(value_in_device_units, __device_number, MAX_DECELERATION_IDX, MAX_DECELERATION_SUB);
	return true;
}

bool MultiDevice::setMaxDeceleration(const std::vector<double> & __max_decelerations)
{
	if ( !checkInputSize(__max_decelerations.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxDeceleration(__max_decelerations.at(i), i);
	return true;
}

void MultiDevice::setMaxDeceleration(const double & __max_deceleration)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxDeceleration(__max_deceleration, i);
}

bool MultiDevice::setDriveName(const std::string & __drive_name, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	if ( !checkDriveName(__drive_name) )
		return false;
	setStringAtDevice(__drive_name, __device_number, DRIVE_NAME_IDX, DRIVE_NAME_SUB);
	return true;
}

bool MultiDevice::setDriveName(const std::vector<std::string> & __drive_names)
{
	if ( !checkInputSize(__drive_names.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		if ( !setDriveName(__drive_names.at(i), i) )
			return false;
	return true;
}

bool MultiDevice::setDriveName(const std::string & __drive_name)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		if ( !setDriveName(__drive_name, i) )
			return false;
	return true;
}

bool MultiDevice::setPositionControlProportionalConstant(const double & __proportional_constant, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __proportional_constant * 1000.0 * 2.0 * M_PI / (encoder_increments_per_turn__.at(__device_number) * rated_torque__.at(__device_number));
	setValueAtDevice<uint32>(value_in_device_units, __device_number, POSITION_CONTROL_KP_IDX, POSITION_CONTROL_KP_SUB);
	return true;
}

bool MultiDevice::setPositionControlProportionalConstant(const std::vector<double> & __proportional_constants)
{
	if ( !checkInputSize(__proportional_constants.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setPositionControlProportionalConstant(__proportional_constants.at(i), i);
	return true;
}

void MultiDevice::setPositionControlProportionalConstant(double & __proportional_constant)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setPositionControlProportionalConstant(__proportional_constant, i);
}

bool MultiDevice::setPositionControlIntegralConstant(const double & __integral_constant, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __integral_constant * 1000.0 * 2.0 * M_PI / (encoder_increments_per_turn__.at(__device_number) * rated_torque__.at(__device_number));
	setValueAtDevice<uint32>(value_in_device_units, __device_number, POSITION_CONTROL_KI_IDX, POSITION_CONTROL_KI_SUB);
	return true;
}

bool MultiDevice::setPositionControlIntegralConstant(const std::vector<double> & __integral_constants)
{
	if ( !checkInputSize(__integral_constants.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setPositionControlIntegralConstant(__integral_constants.at(i), i);
	return true;
}

void MultiDevice::setPositionControlIntegralConstant(const double & __integral_constant)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setPositionControlIntegralConstant(__integral_constant, i);
}

bool MultiDevice::setPositionControlDerivativeConstant(const double & __derivative_constant, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double derivative_constant_in_device_units = __derivative_constant * 1000.0 * 2.0 * M_PI / (encoder_increments_per_turn__.at(__device_number) * rated_torque__.at(__device_number));
	setValueAtDevice<uint32>(derivative_constant_in_device_units, __device_number, POSITION_CONTROL_KD_IDX, POSITION_CONTROL_KD_SUB);
	return true;
}

bool MultiDevice::setPositionControlDerivativeConstant(const std::vector<double> & __derivative_constants)
{
	if ( !checkInputSize(__derivative_constants.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setPositionControlDerivativeConstant(__derivative_constants.at(i), i);
	return true;
}

void MultiDevice::setPositionControlDerivativeConstant(const double & __derivative_constant)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setPositionControlDerivativeConstant(__derivative_constant, i);
}

bool MultiDevice::setVelocityControlProportionalConstant(const double & __proportional_constant, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __proportional_constant * 1000.0 * 2.0 * M_PI / (encoder_increments_per_turn__.at(__device_number) * rated_torque__.at(__device_number));
	setValueAtDevice<uint32>(value_in_device_units, __device_number, VELOCITY_CONTROL_KP_IDX, VELOCITY_CONTROL_KP_SUB);
	return true;
}

bool MultiDevice::setVelocityControlProportionalConstant(const std::vector<double> & __proportional_constants)
{
	if ( !checkInputSize(__proportional_constants.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setVelocityControlProportionalConstant(__proportional_constants.at(i), i);
	return true;
}

void MultiDevice::setVelocityControlProportionalConstant(double & __proportional_constant)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setVelocityControlProportionalConstant(__proportional_constant, i);
}

bool MultiDevice::setVelocityControlIntegralConstant(const double & __integral_constant, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double value_in_device_units = __integral_constant * 1000.0 * 2.0 * M_PI / (encoder_increments_per_turn__.at(__device_number) * rated_torque__.at(__device_number));
	setValueAtDevice<uint32>(value_in_device_units, __device_number, VELOCITY_CONTROL_KI_IDX, VELOCITY_CONTROL_KI_SUB);
	return true;
}

bool MultiDevice::setVelocityControlIntegralConstant(const std::vector<double> & __integral_constants)
{
	if ( !checkInputSize(__integral_constants.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setVelocityControlIntegralConstant(__integral_constants.at(i), i);
	return true;
}

void MultiDevice::setVelocityControlIntegralConstant(const double & __integral_constant)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setVelocityControlIntegralConstant(__integral_constant, i);
}

bool MultiDevice::setVelocityControlDerivativeConstant(const double & __derivative_constant, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double derivative_constant_in_device_units = __derivative_constant * 1000.0 * 2.0 * M_PI / (encoder_increments_per_turn__.at(__device_number) * rated_torque__.at(__device_number));
	setValueAtDevice<uint32>(derivative_constant_in_device_units, __device_number, VELOCITY_CONTROL_KD_IDX, VELOCITY_CONTROL_KD_SUB);
	return true;
}

bool MultiDevice::setVelocityControlDerivativeConstant(const std::vector<double> & __derivative_constants)
{
	if ( !checkInputSize(__derivative_constants.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setVelocityControlDerivativeConstant(__derivative_constants.at(i), i);
	return true;
}

void MultiDevice::setVelocityControlDerivativeConstant(const double & __derivative_constant)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setVelocityControlDerivativeConstant(__derivative_constant, i);
}

bool MultiDevice::setMaxCurrent(const double & __max_current, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double max_current_in_device_units = __max_current * 1000.0 / rated_current__.at(__device_number);
	setValueAtDevice<uint16>(max_current_in_device_units, __device_number, MAX_CURRENT_IDX, MAX_CURRENT_SUB);
	return true;
}

bool MultiDevice::setMaxCurrent(const std::vector<double> & __max_currents)
{
	if ( !checkInputSize(__max_currents.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxCurrent(__max_currents.at(i), i);
	return true;
}

void MultiDevice::setMaxCurrent(const double & __max_current)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxCurrent(__max_current, i);
}

bool MultiDevice::setMaxMotorSpeed(const double & __max_motor_speed, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double max_motor_speed_in_device_units = __max_motor_speed * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<uint32>(max_motor_speed_in_device_units, __device_number, MAX_MOTOR_SPEED_IDX, MAX_MOTOR_SPEED_SUB);
	return true;
}

bool MultiDevice::setMaxMotorSpeed(const std::vector<double> & __max_motor_speeds)
{
	if ( !checkInputSize(__max_motor_speeds.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxMotorSpeed(__max_motor_speeds.at(i), i);
	return true;
}

void MultiDevice::setMaxMotorSpeed(const double & __max_motor_speed)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxMotorSpeed(__max_motor_speed, i);
}

bool MultiDevice::setQuickStopOptionCode(const int16 & __quick_stop_option_code, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	setValueAtDevice<int16>(__quick_stop_option_code, __device_number, QUICK_STOP_OPTION_IDX, QUICK_STOP_OPTION_SUB);
	return true;
}

bool MultiDevice::setQuickStopOptionCode(const std::vector<int16> & __quick_stop_option_codes)
{
	if ( !checkInputSize(__quick_stop_option_codes.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setQuickStopOptionCode(__quick_stop_option_codes.at(i), i);
	return true;
}

void MultiDevice::setQuickStopOptionCode(const int16 & __quick_stop_option_code)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setQuickStopOptionCode(__quick_stop_option_code, i);
}

bool MultiDevice::setPositioningRelativeOption(const uint16 & __option, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	if ( !checkPositioningRelativeOption(__option) )
		return false;
	// to keep the rotary axis option on bits 6 and 7:
	// get old option, put all bits to zero except bits 6 and 7
	// and put new 0 and 1 bits from __option
	uint16 old_option = getPositioningRelativeOption(__device_number);
	uint16 new_option = (old_option & (~0xFF3F)) | __option;
	setValueAtDevice<uint16>(new_option, __device_number, POSITIONING_RELATIVE_OPTION_IDX, POSITIONING_RELATIVE_OPTION_SUB);
	return true;
}

bool MultiDevice::setPositioningRelativeOption(const std::vector<uint16> & __options)
{
	if ( !checkInputSize(__options.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		if ( !setPositioningRelativeOption(__options.at(i), i) )
			return false;
	return true;
}

bool MultiDevice::setPositioningRelativeOption(const uint16 & __option)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		if ( !setPositioningRelativeOption(__option, i) )
			return false;
	return true;
}

bool MultiDevice::setPositioningRotaryAxisOption(const uint16 & __option, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	if ( !checkPositioningRotaryAxisOption(__option) )
		return false;
	// to keep the rotary axis option on bits 6 and 7:
	// get old option, put all bits to zero except bits 6 and 7
	// and put new 0 and 1 bits from __option
	uint16 old_option = getPositioningRotaryAxisOption(__device_number);
	uint16 new_option = (old_option & (~0xFFFC)) | __option;
	setValueAtDevice<uint16>(new_option, __device_number, POSITIONING_ROTARY_AXIS_OPTION_IDX, POSITIONING_ROTARY_AXIS_OPTION_SUB);
	return true;
}

bool MultiDevice::setPositioningRotaryAxisOption(const std::vector<uint16> & __options)
{
	if ( !checkInputSize(__options.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		if ( !setPositioningRotaryAxisOption(__options.at(i), i) )
			return false;
	return true;
}

bool MultiDevice::setPositioningRotaryAxisOption(const uint16 & __option)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		if ( !setPositioningRotaryAxisOption(__option, i) )
			return false;
	return true;
}

bool MultiDevice::setMaxPositionRangeLimit(const double & __max_limit, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double max_limit_in_device_units = __max_limit * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<int32>(max_limit_in_device_units, __device_number, MAX_POSITION_RANGE_LIMIT_IDX, MAX_POSITION_RANGE_LIMIT_SUB);
	return true;
}

bool MultiDevice::setMaxPositionRangeLimit(const std::vector<double> & __max_limits)
{
	if ( !checkInputSize(__max_limits.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxPositionRangeLimit(__max_limits.at(i), i);
	return true;
}

void MultiDevice::setMaxPositionRangeLimit(const double & __max_limit)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxPositionRangeLimit(__max_limit, i);
}

bool MultiDevice::setMinPositionRangeLimit(const double & __min_limit, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	double min_limit_in_device_units = __min_limit * encoder_increments_per_turn__.at(__device_number) / (2.0 * M_PI);
	setValueAtDevice<int32>(min_limit_in_device_units, __device_number, MIN_POSITION_RANGE_LIMIT_IDX, MIN_POSITION_RANGE_LIMIT_SUB);
	return true;
}

bool MultiDevice::setMinPositionRangeLimit(const std::vector<double> & __min_limits)
{
	if ( !checkInputSize(__min_limits.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMinPositionRangeLimit(__min_limits.at(i), i);
	return true;
}

void MultiDevice::setMinPositionRangeLimit(const double & __min_limit)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMinPositionRangeLimit(__min_limit, i);
}

bool MultiDevice::setMaxAllowedTorque(const int & __max_allowed_torque, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	max_allowed_torque__.at(__device_number) = (int16)(__max_allowed_torque);
	return true;
}

bool MultiDevice::setMaxAllowedTorque(const std::vector<int> & __max_allowed_torques)
{
	if ( !checkInputSize(__max_allowed_torques.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxAllowedTorque(__max_allowed_torques.at(i), i);
	return true;
}

void MultiDevice::setMaxAllowedTorque(const int & __max_allowed_torque)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setMaxAllowedTorque(__max_allowed_torque, i);
}

void MultiDevice::setTargetPosition(const std::vector<double> & __target_position)
{
	position_command__ = __target_position;
}

void MultiDevice::setTargetVelocity(const std::vector<double> & __target_velocity)
{
	velocity_command__ = __target_velocity;
}

void MultiDevice::setTargetTorque(const std::vector<double> & __target_torque)
{
	torque_command__ = __target_torque;
}

bool MultiDevice::setTargetPositionRelative(const bool __relative, const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	target_position_relative__.at(__device_number) = __relative;
	return true;
}

bool MultiDevice::setTargetPositionRelative(const std::vector<bool> & __relatives)
{
	if ( !checkInputSize(__relatives.size()) )
		return false;
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setTargetPositionRelative(__relatives.at(i), i);
	return true;
}

void MultiDevice::setTargetPositionRelative(const bool __relative)
{
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
		setTargetPositionRelative(__relative, i);
}

void MultiDevice::setPositionChangeSetImmediately(const bool __change_set_immediately)
{
	position_change_set_immediately__.assign(ec_slavecount, __change_set_immediately);
}

bool MultiDevice::checkDeviceNumber(const unsigned int & __device_number)
{
	if ( ( __device_number < 0 ) || ( __device_number > (ec_slavecount-1) ) )
	{
		if ( verbose__ )
			std::cout << "ERROR\tIncorrect device number" << std::endl;
		return false;
	}
	return true;
}

bool MultiDevice::checkInputSize(const int & __size)
{
	if ( __size != ec_slavecount )
	{
		if ( verbose__ )
			std::cout << "ERROR\tIncorrect value size, should match the number of devices" << std::endl;
		return false;
	}
	return true;
}

bool MultiDevice::checkDriveName(const std::string & __drive_name)
{
	if ( __drive_name.size() > 8 )
	{
		if ( verbose__ )
			std::cout << "ERROR\tDrive name can not be more than 8 characters long" << std::endl;
		return false;
	}
	return true;
}

bool MultiDevice::checkPositioningRelativeOption(const uint16 & __option)
{
	if ( (__option != 0) && (__option != 1) && (__option != 2) )
	{
		if ( verbose__ )
			std::cout << "ERROR\tInvalid positioning relative option" << std::endl;
		return false;
	}
	return true;
}

bool MultiDevice::checkPositioningRotaryAxisOption(const uint16 & __option)
{
	if ( (__option != 0) && (__option != 64) && (__option != 128) && (__option != 192) )
	{
		if ( verbose__ )
			std::cout << "ERROR\tInvalid positioning rotary axis option" << std::endl;
		return false;
	}
	return true;
}

}
